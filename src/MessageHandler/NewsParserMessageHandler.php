<?php

namespace App\MessageHandler;

use App\Entity\Article;
use App\Message\NewsParserMessage;
use DateTimeImmutable;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsMessageHandler]
class NewsParserMessageHandler
{
    private HttpClientInterface $client;
    private ManagerRegistry $doctrine;

    public function __construct(HttpClientInterface $client, ManagerRegistry $doctrine)
    {
        $this->client = $client->withOptions([
            'headers' => [
                'X-RapidAPI-Key' => 'bb256b13f7msh5178e99b47cfbe1p1e309cjsn2a5fc51d1048',
                'X-RapidAPI-Host'=> 'contextualwebsearch-websearch-v1.p.rapidapi.com'
                ]
        ]);

        $this->doctrine = $doctrine;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function __invoke(NewsParserMessage $message): void
    {
        $response = $this->client->request(
            'GET',
            'https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/search/NewsSearchAPI',
            [
                'query' => [
                    'q'=> $message,
                    'pageNumber'=>1,
                    'pageSize' => 100,
                    'autoCorrect'=>true
                ]
            ]
        );

        if($response->getStatusCode() == 200){
            $content = $response->toArray();
            $entityManager = $this->doctrine->getManager();

            $results = $content["value"];
            foreach ($results as $result) {
                $article =  new Article();
                $article->setTitle($result["title"]);
                $article->setDescription($result['description']);
                $article->setPicture($result['image']['url']);
                $article->setCreated(new DateTimeImmutable($result['datePublished']));

                $entityManager->persist($article);
            }

            $entityManager->flush();

        }

    }

}